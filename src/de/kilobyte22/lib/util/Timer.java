package de.kilobyte22.lib.util;

import de.kilobyte22.lib.event.EventBus;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 10.03.13
 * Time: 14:02
 * To change this template use File | Settings | File Templates.
 */
public class Timer {
    private long millis;
    private EventBus eventBus;
    private State state = State.STOPPED;
    private Timer parent = this;
    private Thread runner;

    public Timer(long millis, EventBus eventBus) {

        this.millis = millis;
        this.eventBus = eventBus;
    }

    public void run() {
        if (state != State.STOPPED) {
            state = State.RESTARTED;
            runner.interrupt();
        } else {
            state = State.RUNNING;
            runner = new Thread(new TimerRunner());
            runner.setDaemon(true); // Do not prevent Bot Shutdown
            runner.start();
        }

    }

    public void abort() {
        if (state != State.STOPPED) {
            state = State.ABORTED;
            runner.interrupt();
        }
    }

    public boolean isRunning() {
        return state == State.RUNNING || state == State.RESTARTED;
    }

    private class TimerRunner implements Runnable {

        @Override
        public void run() {
            while (true) {
                try {
                    Thread.sleep(millis);
                    eventBus.postLocal(new TimerEvent(parent));
                    state = State.STOPPED;
                    return;
                } catch (InterruptedException e) {
                    if (state == State.ABORTED) {
                        eventBus.postLocal(new TimerAbortedEvent(parent));
                        state = State.STOPPED;
                        return;
                    } else if(state == State.RESTARTED) {
                        state = State.RUNNING;
                    } else {
                        state = State.STOPPED;
                        return;
                    }
                }
            }
        }
    }

    private enum State {
        STOPPED,
        RUNNING,
        ABORTED,
        RESTARTED
    }
}
