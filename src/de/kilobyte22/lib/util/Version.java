package de.kilobyte22.lib.util;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 08.03.13
 * Time: 11:11
 *
 * @author Stephan
 * @copyright Copyright 2013 Stephan
 */
public class Version implements Comparable {
    private int major;
    private int minor;
    private int revision;
    private int build;

    public Version(int major, int minor, int revision, int build) {

        this.major = major;
        this.minor = minor;
        this.revision = revision;
        this.build = build;
    }

    public Version(String version) {
        this(0, 0, 0, 0);
        if (version.isEmpty())
            throw new IllegalArgumentException("version must not be empty");
        String[] ver = version.split("\\.");
        try {
            major = Integer.valueOf(ver[0]);
            minor = Integer.valueOf(ver[1]);
            revision = Integer.valueOf(ver[2]);
            build = Integer.valueOf(ver[3]);
        } catch (ArrayIndexOutOfBoundsException ex) {

        }
    }

    public int getMajor() {
        return major;
    }

    public int getMinor() {
        return minor;
    }

    public int getRevision() {
        return revision;
    }

    public int getBuild() {
        return build;
    }

    @Override
    public boolean equals(Object anotherObject) {
        if (anotherObject == null) return false;
        if (!(anotherObject instanceof Version))
            return false;
        Version v = (Version) anotherObject;
        return v.major == major && v.minor == minor && v.revision == revision && v.build == build;
    }


    @Override
    public int compareTo(Object o) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
