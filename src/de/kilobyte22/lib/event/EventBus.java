package de.kilobyte22.lib.event;

import de.kilobyte22.lib.exceptions.ObjectLockedException;

import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 07.03.13
 * Time: 18:41
 *
 * @author Stephan Henrichs
 * @copyright Copyright 2013 Stephan Henrichs
 */
public class EventBus extends com.google.common.eventbus.EventBus {
    protected EventBus parent = null;
    protected boolean isFrozen = false;
    private boolean isLocked = false;

    public EventBus() {
        super();
    }

    public EventBus(String identifier) {
        super(identifier);
    }

    public EventBus(EventBus parent) {
        this();
        parent.childs.add(this);
    }

    public EventBus(String identifier, EventBus parent) {
        this(identifier);
        parent.childs.add(this);
    }

    protected LinkedList<EventBus> childs = new LinkedList<EventBus>();

    public void dispose() {
        checkLock();
        if (parent != null)
            parent.childs.remove(this);
        for (EventBus child : childs) {
            parent = null;
        }
    }

    @Override
    public void post(Object event) {
        post(event, null);
    }

    public void postLocal(Object event) {
        super.post(event);
    }

    protected void post(Object event, EventBus sender) {
        if (sender != null && isFrozen) return;
        super.post(event);
        if (isFrozen) return;
        for (EventBus bus : childs) {
            if (bus != sender)
                bus.post(event, this); // make sure no infinite recursion happens
        }
        if (parent != null && sender != parent)
            parent.post(event, this);
    }

    /**
     * Freezes the bus. in other words, it won't distribute events to childs and parents
     *
     * @param value
     */
    public void setFrozen(boolean value) {
        checkLock();
        isFrozen = value;
    }

    public boolean getFrozen() {
        return isFrozen;
    }

    public boolean getLocked() {
        return isLocked;
    }

    private void checkLock() {
        if (isLocked)
            throw new ObjectLockedException();
    }

    public class LockController {
        public LockController() {
            checkLock();
        }
        public void setLockState(boolean state) {
            isLocked = state;
        }
        public void setFrozen(boolean state) {
            isFrozen = state;
        }
        public void dispose() {
            if (parent != null)
                parent.childs.remove(this);
            for (EventBus child : childs) {
                parent = null;
            }
        }
        @Override
        public void finalize() {
            isLocked = false;
        }
    }
}
