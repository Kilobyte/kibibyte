package de.kilobyte22.lib.logging;

import com.google.common.eventbus.EventBus;

import java.io.PrintStream;

import static de.kilobyte22.lib.logging.LogLevel.INFO;
import static de.kilobyte22.lib.logging.LogLevel.SEVERE;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 16.01.13
 * Time: 20:10
 * To change this template use File | Settings | File Templates.
 */
public class Logger {
    static {
        //Class.forName("de.kilobyte22.lib.logging.LogLevel"); // Make sure the LogLevels initialize before replacing any streams
        stdout = System.out;
        stderr = System.err;
        System.setOut(new LogStream(System.out, new Logger("STDOUT"), INFO));
        System.setErr(new LogStream(System.out, new Logger("STDERR"), SEVERE));
    }
    static EventBus eventBus;
    static PrintStream stdout, stderr;
    Logger parent;
    String name;
    private LogStream stream;

    public Logger() {
        this(null);
    }

    public static void setEventBus(EventBus eb) {
        eventBus = eb;
    }

    public Logger(String name) {
        this(name, null);
    }

    public void setStream(LogStream st) {
        stream = st;
    }

    public Logger(String name, Logger parent) {
        this.name = name;
        this.parent = parent;
    }

    public void log(LogLevel level, String message) {
        if (parent != null)
            parent.log(level, message);
        else {
            String outline = (name != null ? "[" + name + "] " : "") + message;
            if (eventBus != null) {
                LogEvent e = new LogEvent();
                e.level = level;
                e.logger = this;
                e.message = message;
                e.printmessage = outline;
                eventBus.post(e);
                outline = e.printmessage;
            }
            level.writeLine(outline);
        }
    }

    public void log(String message) {
        log(INFO, message);
    }

    public void log(Throwable ex) {
        log(LogLevel.SEVERE, ex.getClass().getName() + ": " + ex.getMessage());
        for (StackTraceElement el : ex.getStackTrace()) {
            log(LogLevel.SEVERE, el.toString());
        }
    }

    public LogStream getStream() {
        return stream;
    }
}
