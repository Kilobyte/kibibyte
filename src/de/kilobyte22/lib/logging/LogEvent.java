package de.kilobyte22.lib.logging;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 03.03.13
 * Time: 14:42
 *
 * @author Stephan
 * @copyright Copyright 2013 Stephan
 */
public class LogEvent {
    public Logger logger;
    public String message;
    public String printmessage;
    public LogLevel level;
}
