package de.kilobyte22.plugin.kibi.test1;

import de.kilobyte22.app.kibibyte.command.Command;
import de.kilobyte22.app.kibibyte.command.CommandHandler;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 07.03.13
 * Time: 20:55
 *
 * @author Stephan
 * @copyright Copyright 2013 Stephan
 */
public class Commands extends CommandHandler {
    @Command(name = "test_", usage = "", help = "")
    public void test() {
        print("YAY");
    }
}
