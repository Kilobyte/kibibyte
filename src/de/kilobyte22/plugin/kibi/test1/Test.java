package de.kilobyte22.plugin.kibi.test1;

import com.google.common.eventbus.Subscribe;
import de.kilobyte22.app.kibibyte.events.EnableEvent;
import de.kilobyte22.app.kibibyte.plugin.BotAccess;
import de.kilobyte22.app.kibibyte.plugin.IPlugin;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 07.03.13
 * Time: 19:54
 *
 * @author Stephan
 * @copyright Copyright 2013 Stephan
 */
public class Test implements IPlugin {
    BotAccess botAccess;
    @Override
    public void onLoad(BotAccess bot) {
        botAccess = bot;
        bot.commandManager.registerHandler(Commands.class);
    }

    @Override
    public void onUnload() {
    }

    // Start of optional part

    @Subscribe
    public void onEnable(EnableEvent event) {

    }
}
