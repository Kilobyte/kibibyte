package de.kilobyte22.plugin.kibi.consolechat;

import com.google.common.eventbus.Subscribe;
import de.kilobyte22.app.kibibyte.plugin.BotAccess;
import de.kilobyte22.app.kibibyte.plugin.IPlugin;
import org.pircbotx.hooks.events.MessageEvent;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 10.03.13
 * Time: 16:28
 * To change this template use File | Settings | File Templates.
 */
public class ConsoleChat implements IPlugin {
    Scanner scanner = new Scanner(System.in);
    BotAccess botAccess;
    Thread reader;

    @Override
    public void onLoad(BotAccess bot) {
        botAccess = bot;
        bot.eventBus.register(this);
    }

    @Override
    public void onUnload() {

    }

    @Subscribe
    public void onMessage(MessageEvent event) {
        botAccess.logger.log("[" + event.getChannel().getName() + "] <" + event.getUser().getNick() + "> " + event.getMessage());
    }

    private class BackGroundWorker implements Runnable {
        @Override
        public void run() {
            while (!reader.isInterrupted()) {
                String line = scanner.nextLine();
                if (reader.isInterrupted())
                    return;
                if (line.startsWith("/")) {
                    // command
                }

            }
        }
    }
}
