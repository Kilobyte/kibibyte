package de.kilobyte22.app.kibibyte.events;

import org.pircbotx.User;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 03.03.13
 * Time: 14:37
 *
 * @author Stephan
 * @copyright Copyright 2013 Stephan
 */
public class RehashEvent {
    public RehashEvent(User sender) {
        rehasher = sender;
    }
    public User rehasher;
    private ArrayList<String> warnings = new ArrayList<String>();
    public void warn(String message) {
        warnings.add(message);
    }
}
