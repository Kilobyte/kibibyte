package de.kilobyte22.app.kibibyte;

import de.kilobyte22.lib.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 03.03.13
 * Time: 13:20
 *
 * @author Stephan
 * @copyright Copyright 2013 Stephan
 */
public class Start {

    public static void main(String[] args) {
        new Kibibyte().run(args);
    }
}
