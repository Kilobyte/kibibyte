package de.kilobyte22.app.kibibyte.exceptions;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 07.03.13
 * Time: 14:23
 *
 * @author Stephan
 * @copyright Copyright 2013 Stephan
 */
public class PluginNotFoundException extends Exception {
}
