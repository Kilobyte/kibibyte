package de.kilobyte22.app.kibibyte.misc;


import de.kilobyte22.app.kibibyte.exceptions.UsageException;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 16.02.13
 * Time: 12:23
 *
 * @author ${user}
 * @copyright Copyright ${year} ${user}
 */
public class Args {
    public Args(HashMap<Object, String> args) {
        this.args = args;
    }
    HashMap<Object, String> args;

    public String getOrError(int param) {
        String ret = args.get(param);
        if (ret == null)
            throw new UsageException();
        return ret;
    }

    public String get(int param) {
        return args.get(param);
    }

    public String getNamedParam(String param) {
        return args.get("--" + param);
    }

    public String getNamedParam(char param) {
        return args.get("-" + param);
    }

    public int getIntOrError(int param) {
        try {
            return Integer.valueOf(getOrError(param));
        } catch (Exception ex) {
            throw new UsageException();
        }
    }
}
