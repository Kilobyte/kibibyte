package de.kilobyte22.app.kibibyte.misc;

import de.kilobyte22.app.kibibyte.Database;
import de.kilobyte22.app.kibibyte.Kibibyte;
import de.kilobyte22.lib.logging.LogLevel;
import de.kilobyte22.lib.logging.Logger;
import org.pircbotx.Channel;
import org.pircbotx.User;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 17.02.13
 * Time: 13:25
 *
 * @author Stephan
 * @copyright Copyright 2013 Stephan
 */
public class Permission {
    Kibibyte bot;
    private Logger logger = new Logger("PERMISSIONSYS");
    private HashMap<String, HashMap<String, Boolean>> permcache = new HashMap<String, HashMap<String, Boolean>>();

    public Permission(Kibibyte b) {
        bot = b;
    }

    public boolean hasParsedPermission(String permission, String nick, Channel channel) {
        if (permission.equals(""))
            return true;
        Boolean perm = hasPermission(permission, "$n:" + nick, channel);
        if (perm)
            return perm;
        String acc = bot.nickservSystem.getAccount(nick);
        if (acc == null)
            return false;
        perm = hasPermission(permission, acc, channel);
        logger.log(perm ? "Has" : "Has not");
        return perm;
        //return hasPermission(permission, nick, channel);
    }

    public boolean hasPermission(String permission, String nick, Channel channel) {
        Boolean has = hasPermissionImpl(permission, nick, channel);
        if (has != null)
            return has;
        has = hasPermissionImpl(permission, ":default", channel);
        if (has != null)
            return has;
        return false;
    }

    private Boolean hasPermissionImpl(String permission, String nick, Channel channel) {
        HashMap<String, Boolean> perms = getPermissions(nick);
        String node = permission;
        /*for (String s : perms.keySet()) {
            log.log(LogLevel.DEBUG, s + " is " + (perms.get(s) != null ? (perms.get(s) ? "given" : "not given") : "not set"));
        }*/
        do {
            String realnode = node + ((permission.equalsIgnoreCase(node) || node.equals("*")) ? "" : ".*");
            //logger.log("Getting perm " + realnode + " on " + nick);
            if (perms.get(realnode) != null) return perms.get(realnode);
            //log.log(LogLevel.DEBUG, realnode + " is " + (perms.get(realnode) != null ? (perms.get(realnode) ? "given" : "not given") : "not set"));
            String[] nodehacked = node.split("\\.");
            try {
                node = node.substring(0, node.length() - nodehacked[nodehacked.length - 1].length() - 1);
            } catch (Exception ex) {
                node = (node.equals("*") ? "" : "*");
            }
        } while (!( node.equals("")));
        return null;
    }

    public HashMap<String, Boolean> getPermissions(String nick) {
        /*HashMap <String, Boolean> tmp = permcache.get(nick);
        if (tmp != null) return tmp;*/
        if (bot.useYaml) {
            try {
                HashMap<String, String> allPerms = bot.yamlEngine.load(new File("permissions.yaml"));
                String perms = allPerms.get(nick);
                if (perms == null) return new HashMap<String, Boolean>();
                String[] permsSet = perms.split(";");
                HashMap<String, Boolean> ret = new HashMap<String, Boolean>();
                for(String s2 : permsSet) {
                    String[] perm = s2.split("=");
                    ret.put(perm[0], (perm[1].equalsIgnoreCase("true") ? true : false));
                }
                permcache.put(nick, ret);
                return ret;
            } catch (Exception ex) {
                logger.log(LogLevel.SEVERE, "Could not get Permission: " + ex.getClass().getName() + ": " + ex.getMessage());
                return new HashMap<String, Boolean>();
                //return tmp;*/
            }
        } else {
            String query = "SELECT * FROM permissions WHERE nickserv = ?";
            //logger.log("Query: " + query + " Server: " + bot.bot.getServer() + " Nick: " + nick);

            PreparedStatement s = bot.database.prepareStatement(query);
            try {
                //s.setString(1, bot.getServer());
                s.setString(1, nick);
                ResultSet res = s.executeQuery();
                res.next();
                HashMap<String, Boolean> perms = new HashMap<String, Boolean>();
                String[] permsSet = res.getString("permissions").split(";");
                for(String s2 : permsSet) {
                    String[] perm = s2.split("=");
                    perms.put(perm[0], (perm[1].equalsIgnoreCase("true") ? true : false));
                }
                //logger.log(perms.toString());
                //permcache.put(nick, perms);
                return perms;
            } catch (Exception ex) {
                logger.log(LogLevel.SEVERE, "Could not get Permission: " + ex.getClass().getName() + ": " + ex.getMessage());
                //logger.log(ex);
                //ex.printStackTrace();
                return new HashMap<String, Boolean>();
                //permcache.put(nick, tmp);
                //return tmp;
            }
        }
    }
    public void setPermissions(String nick, HashMap<String, Boolean> perms) {
        Database database = bot.database;
        String tmpperms = "";
        for(String key : perms.keySet()) {
            if (!tmpperms.equals("")) tmpperms += ";";
            if (perms.get(key) != null) tmpperms += key + "=" + (perms.get(key) ? "true" : "false");
        }
        logger.log(tmpperms);
        try {
            if (bot.useYaml) {
                HashMap<String, String> perms_ = bot.yamlEngine.load(new File("permissions.yaml"));
                perms_.put(nick, tmpperms);
                bot.yamlEngine.writeYaml(new File("permissions.yaml"), perms_);
            } else {
                PreparedStatement s = database.prepareStatement("SELECT * FROM permissions WHERE server = ? AND nickserv = ?");
                s.setString(1, bot.getServer());
                s.setString(2, nick);
                try {
                    //s.executeQuery().next();
                    s = database.prepareStatement("UPDATE permissions SET permissions=? WHERE server = ? AND nickserv = ?");
                    s.setString(1, tmpperms);
                    s.setString(2, bot.getServer());
                    s.setString(3, nick);
                    if (!s.execute())
                        throw new Exception("");
                    logger.log(s.toString());
                } catch (Exception ex) {
                    //logger.log(ex);
                    //s.executeQuery().next();
                    s = database.prepareStatement("INSERT INTO permissions (server, nickserv, permissions) VALUES (?, ?, ?)");
                    s.setString(1, bot.getServer());
                    s.setString(2, nick);
                    s.setString(3, tmpperms);
                    s.execute();
                }
            }
        } catch (Exception ex) {
            //logger.log(LogLevel.WARNING, "Could not set permissions");
            //logger.log(ex);
            logger.log(LogLevel.SEVERE, "Could not set Permission: " + ex.getClass().getName() + ": " + ex.getMessage());
        }

    }
}
