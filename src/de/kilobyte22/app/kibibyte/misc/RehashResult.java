package de.kilobyte22.app.kibibyte.misc;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 03.03.13
 * Time: 14:34
 *
 * @author Stephan
 * @copyright Copyright 2013 Stephan
 */
public class RehashResult {
    public enum Result {
        SUCCESS,
        FAIL
    }
    private ArrayList<String> warnings;
    public String error;
    public Result result = Result.SUCCESS;

    public RehashResult() {

    }
}
