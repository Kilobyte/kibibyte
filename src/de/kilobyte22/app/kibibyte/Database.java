package de.kilobyte22.app.kibibyte;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 20.01.13
 * Time: 18:41
 *
 * @author ${user}
 * @copyright Copyright ${year} ${user}
 */


import de.kilobyte22.lib.logging.LogLevel;
import de.kilobyte22.lib.logging.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;


public class Database
{
    private Connection connection = null;
    static Logger log = new Logger("DATABASE");
    public Database(String driver, String connectionString)
    {
        try
        {
            log.log(LogLevel.INFO, "Initializing Database...");
            Class.forName(driver); // mysql: "com.mysql.jdbc.Driver"
            //connection = DriverManager.getConnection("jdbc:mysql://" + host + (port == 0 ? "" : ":" + port) + "/" + db + "?user=" + user + (pass.equals("") ? "" : "&password=" + pass));*/
            connection = DriverManager.getConnection(connectionString);
            log.log(LogLevel.INFO, "Done.");
        }
        catch (Exception ex)
        {
            log.log(LogLevel.SEVERE, "Error Starting database:");
            log.log(ex);
            throw new RuntimeException(ex);
        }

    }

    public PreparedStatement prepareStatement(String query) {
        try {
            return connection.prepareStatement(query);
        } catch (Exception ex) {
            log.log(LogLevel.SEVERE, "Cound not prepare Statement: " + query);
            ex.printStackTrace();
            return null;
        }
    }

    public ResultSet performQuery(String query, Object... params)
    {
        try {
            Statement statement = connection.createStatement();
            return statement.executeQuery(query);
        } catch (Exception ex) {
            log.log(LogLevel.SEVERE, "Could not perform query: " + query);
            ex.printStackTrace();
            return null;
        }
    }
}

