package de.kilobyte22.app.kibibyte;

import de.kilobyte22.app.kibibyte.command.BasicCommands;
import de.kilobyte22.app.kibibyte.command.CommandManager;
import de.kilobyte22.app.kibibyte.events.RehashEvent;
import de.kilobyte22.app.kibibyte.exceptions.InvalidPluginException;
import de.kilobyte22.app.kibibyte.exceptions.PluginAlreadyLoadedException;
import de.kilobyte22.app.kibibyte.exceptions.PluginNotFoundException;
import de.kilobyte22.app.kibibyte.misc.Nickserv;
import de.kilobyte22.app.kibibyte.misc.Permission;
import de.kilobyte22.app.kibibyte.misc.RehashResult;
import de.kilobyte22.app.kibibyte.misc.Utils;
import de.kilobyte22.app.kibibyte.plugin.PluginManager;
import de.kilobyte22.lib.config.Configuration;
import de.kilobyte22.lib.event.EventBus;
import de.kilobyte22.lib.logging.Logger;
import de.kilobyte22.lib.util.Version;import org.pircbotx.PircBotX;
import org.pircbotx.User;
import org.pircbotx.exception.IrcException;
import org.pircbotx.hooks.Event;
import org.pircbotx.hooks.Listener;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 03.03.13
 * Time: 14:25
 *
 * @author Stephan
 * @copyright Copyright 2013 Stephan
 */
public class Kibibyte extends PircBotX {

    public static final Version VERSION = new Version(1, 0, 0, 0);

    static Logger logger = new Logger("MAIN");
    public Database database;
    public Configuration config;
    public EventBus eventBus = new EventBus();
    public CommandManager commandManager = new CommandManager(this);
    public PluginManager pluginManager;
    public YamlEngine yamlEngine = new YamlEngine();

    public String server, nick, serverpass, nspass, operpass, opername, realname;
    public String[] channels;
    public int port;
    public boolean verbose, crash, useYaml;

    public Nickserv nickservSystem = new Nickserv(this);
    public String[] prefixes;
    public Permission permissionSystem = new Permission(this);
    private String botowner;

    public void run(String[] args) {
        logger.log("Starting...");
        {
            File f = new File("config");
            if (!f.exists())
                f.mkdir();
        }
        eventBus.register(this);
        Logger.setEventBus(eventBus);
        pluginManager = new PluginManager(this);
        config = new Configuration(new File("kibibyte.conf"));
        rehash(null);
        if (crash) {
            logger.log("Crash Option enabled, stopping");
            System.exit(-1);
        }
        //bot = new PircBotX();
        this.setVerbose(verbose);
        this.setName(nick);
        logger.log("Connecting...");
        this.setLogin("kibibyte");
        this.setVersion("Kibibyte version 5.1 - owned by " + botowner);
        try {
            this.connect(server, port, (serverpass.equals("") ? null : serverpass));
            if (nspass != null && !nspass.equals(""))
                this.sendMessage("NickServ", "identify " + nspass);
            if (opername != null && !opername.equals("") && operpass != null && !operpass.equals(""))
                this.sendRawLine("OPER " + opername + " " + operpass);
        } catch (IOException e) {
            logger.log(e);
        } catch (IrcException e) {
            logger.log(e);
        }

        this.getListenerManager().addListener(new Listener() {
            @Override
            public void onEvent(Event event) throws Exception {
                eventBus.post(event);
            }
        });
        commandManager.registerHandler(BasicCommands.class);

        Utils.sleep(500);

        logger.log("Joining...");

        for (String c : channels)
            joinChannel(c);
        logger.log("Done Starting.");
    }

    public RehashResult rehash(User user) {
        String nick;
        if (user == null)
            nick = "Console";
        else
            nick = user.getNick();
        logger.log(nick + " is rehashing config...");
        config.load();
        RehashResult res = new RehashResult();
        try {
            database = new Database(
                    config.get("Database", "driver", "", "The driver to use. For MySql use 'com.mysql.jdbc.Driver'").value,
                    config.get("Database", "conectionstring", "", "The database to connect to. For a MySql connection use 'jdbc:mysql://<host>[:<port]/<database>?user=<user>&password=<pass>'").value
            );
        } catch (Exception ex) {
            res.result = RehashResult.Result.FAIL;
            res.error = "Failed to reload Database";
        }

        channels = config.get("connection", "channels", "#kibibyte", "What channels should Kibi hang around in?").value.split(",");
        server = config.get("connection", "server", "irc.example.net", "The server to connect to").value;
        this.nick = config.get("connection", "nick", "Kibibyte", "The nick to use").value;
        port = config.get("connection", "port", 6667, "The port of the server").getInt();
        //realname = config.get("connection", "realname", "kibibyte - An IRC Casino by Kilobyte", "The realname for the bot").value;
        serverpass = config.get("connection", "password", "", "The serverpassword. Leave empty for none").value;
        nspass = config.get("connection", "nickserv password", "", "The password for the bots nickserv account. Leave empty to disable").value;
        opername = config.get("connection", "oper name", "", "The oper name. Leave empty if your bot has no oline").value;
        operpass = config.get("connection", "oper password", "", "The oper password. Leave empty if your bot has no oline").value;

        verbose = config.get("general", "verbose", false, "Should PIrcBotX put out verbose outputs?").getBoolean(false);
        crash = config.get("general", "killbot", true, "Disable this. this is to prevent any issues when starting the bot the first time by stopping it after loading the config").getBoolean(true);
        prefixes = config.get("general", "prefixes", ". ! ++", "Space seperated list of prefixes").value.split(" ");
        botowner = config.get("general", "owner", "Kilobyte", "Put your nick here").value;
        useYaml = config.get("general", "storageFormat", "sql", "Currently avaible: sql, yaml").value.equals("yaml");

        setMessageDelay(config.get("connection", "messagedelay", 1000, "The delay in milliseconds between 2 messages").getInt());
        if (this.isConnected())
            setVerbose(verbose);

        String[] autoplugins = config.get("general", "autoload plugins", "", "Space separated list of plugins to autoload").value.split(" ");
        for (String s : autoplugins) {
            if (!s.equals(""))
            try {
                pluginManager.load(s);
            } catch (PluginNotFoundException e) {
                e.printStackTrace();
            } catch (InvalidPluginException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (PluginAlreadyLoadedException e) {
                e.printStackTrace();
            }
        }

        eventBus.post(new RehashEvent(user));
        config.save();
        logger.log("Rehash done.");
        return res;
    }
}
