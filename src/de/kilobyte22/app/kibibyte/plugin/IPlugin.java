package de.kilobyte22.app.kibibyte.plugin;

import de.kilobyte22.app.kibibyte.Kibibyte;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 07.03.13
 * Time: 08:02
 *
 * @author Stephan
 * @copyright Copyright 2013 Stephan
 */
public interface IPlugin {
    /**
     * Called when plugin gets loaded
     */
    public void onLoad(BotAccess bot);

    /**
     * Called before a plugin gets unloaded
     */
    public void onUnload();

}
