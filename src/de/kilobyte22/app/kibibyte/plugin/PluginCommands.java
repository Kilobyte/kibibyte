package de.kilobyte22.app.kibibyte.plugin;

import de.kilobyte22.app.kibibyte.command.Command;
import de.kilobyte22.app.kibibyte.command.CommandHandler;
import de.kilobyte22.app.kibibyte.exceptions.CommandException;
import de.kilobyte22.app.kibibyte.exceptions.InvalidPluginException;
import de.kilobyte22.app.kibibyte.exceptions.PluginAlreadyLoadedException;
import de.kilobyte22.app.kibibyte.exceptions.PluginNotFoundException;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 07.03.13
 * Time: 08:04
 *
 * @author Stephan
 * @copyright Copyright 2013 Stephan
 */
public class PluginCommands extends CommandHandler {
    @Command(name = "load", usage = "[--class/-c <class>] <name>", help = "Loads a plugin", permission = "cmd.plugins.load")
    public void load() {
        String clazz = args.getNamedParam('c');
        if (clazz == null) clazz = args.getNamedParam("class");
        if (clazz == null) {
            try {
                bot.pluginManager.load(args.getOrError(1));
                print("Done.");
            } catch (PluginNotFoundException e) {
                throw new CommandException("Unknown Plugin");
            } catch (InvalidPluginException e) {
                throw new CommandException("Invalid Plugin:" + e.getMessage());
            } catch (ClassNotFoundException e) {
                throw new CommandException("Plugin Class not found");
            } catch (IOException e) {
                throw new CommandException("IOException: " + e.getMessage());
            } catch (InstantiationException e) {
                throw new CommandException("Could not create plugin: " + e.getMessage());
            } catch (IllegalAccessException e) {
                throw new CommandException("Constructor was private");
            } catch (PluginAlreadyLoadedException e) {
                throw new CommandException("Plugin already loaded");
            }
        } else {
            try {
                bot.pluginManager.loadInternal(clazz, args.getOrError(1));
                if (args.getNamedParam('n') == null && args.getNamedParam("noenable") == null)
                    bot.pluginManager.enable(args.getOrError(1));
                print("Done.");
            } catch (IllegalAccessException e) {
                throw new CommandException("Could not access constructor for Plugin");
            } catch (InstantiationException e) {
                throw new CommandException("Could not create new Plugin");
            } catch (ClassNotFoundException e) {
                throw new CommandException("Invalid Class");
            } catch (PluginAlreadyLoadedException e) {
                throw new CommandException("Plugin already loaded");
            } catch (InvalidPluginException e) {
                throw new CommandException("Invalid plugin: " + e.getMessage());
            } catch (PluginNotFoundException e) {
                throw new CommandException("Plugin does not exist");
            }
        }
    }

    @Command(name = "enable", usage = "<plugin>", help = "Enables a plugin", permission = "cmd.plugins.enable")
    public void enable() {
        try {
            bot.pluginManager.enable(args.getOrError(1));
            print("Done.");
        } catch (PluginNotFoundException e) {
            throw new CommandException("Plugin not found");
        }
    }

    @Command(name = "disable", usage = "<plugin>", help = "Disables a plugin", permission = "cmd.plugins.disable")
    public void disable() {
        try {
            bot.pluginManager.disable(args.getOrError(1));
            print("Done.");
        } catch (PluginNotFoundException e) {
            throw new CommandException("Plugin not found");
        }
    }

    @Command(name = "unload", usage = "<plugin>", help = "Unloads a plugin", permission = "cmd.plugins.unload")
    public void unload() {
        try {
            bot.pluginManager.unload(args.getOrError(1));
            print("Done.");
        } catch (PluginNotFoundException e) {
            throw new CommandException("Plugin not found");
        }
    }

    @Command(name = "updatepluginlist", usage = "", help = "Updates the plugin list", permission = "cms.plugins.updatelist")
    public void updateList() {
        bot.pluginManager.refreshPluginList();
        int count = bot.pluginManager.getPluginList().size();
        print("Done. " + count + " Plugin" + (count == 1 ? "" : "s") + " found");
    }
}
