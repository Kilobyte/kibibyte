package de.kilobyte22.app.kibibyte.plugin;

import de.kilobyte22.app.kibibyte.Kibibyte;
import de.kilobyte22.app.kibibyte.command.CommandManager;
import de.kilobyte22.app.kibibyte.misc.Nickserv;
import de.kilobyte22.app.kibibyte.misc.Permission;
import de.kilobyte22.lib.config.Configuration;
import de.kilobyte22.lib.event.EventBus;
import de.kilobyte22.lib.logging.Logger;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 07.03.13
 * Time: 18:36
 *
 * @author Stephan
 * @copyright Copyright 2013 Stephan
 */
public class BotAccess {
    public Configuration config;
    public EventBus eventBus;
    public Permission permissionSystem;
    public Nickserv nickservSystem;
    public CommandManager commandManager;
    public Logger logger;
    /**
     * Do not use unless there is no other way! this will go away later
     */
    @Deprecated public Kibibyte kibibyte; // Do not use unless necessary, it will probably go away

    private EventBus.LockController eventBusLock;
    private Plugin plugin;
    private Kibibyte bot;

    public BotAccess(Kibibyte bot, Plugin plugin) {
        this.plugin = plugin;
        this.bot = bot;
        this.kibibyte = bot;

        eventBus = new EventBus(bot.eventBus);
        plugin.setEventBus(eventBus);
        eventBusLock = eventBus.new LockController();
        eventBusLock.setLockState(true); // BAM
        eventBusLock.setFrozen(true);

        permissionSystem = bot.permissionSystem;
        nickservSystem = bot.nickservSystem;
        commandManager = new CommandManager(bot.commandManager);
        commandManager.setEnabled(false);

        config = new Configuration(new File("config/" + plugin.getName() + ".cfg"));
        logger = new Logger("PLUGIN/" + plugin.getName());
    }
}
