package de.kilobyte22.app.kibibyte.plugin;

import de.kilobyte22.app.kibibyte.Kibibyte;
import de.kilobyte22.app.kibibyte.exceptions.InvalidPluginException;
import de.kilobyte22.app.kibibyte.exceptions.PluginAlreadyLoadedException;
import de.kilobyte22.app.kibibyte.exceptions.PluginNotFoundException;
import de.kilobyte22.lib.logging.Logger;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.jar.JarFile;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 07.03.13
 * Time: 07:59
 *
 * @author Stephan
 * @copyright Copyright 2013 Stephan
 */
public class PluginManager {
    Kibibyte bot;
    Logger logger = new Logger("PLUGINMGR");
    File pluginDirectory;
    HashMap<String, Plugin> plugins = new HashMap<String, Plugin>();
    HashMap<String, File> allPlugins = new HashMap<String, File>();

    public PluginManager(Kibibyte bot) {
        this.bot = bot;
        bot.commandManager.registerHandler(PluginCommands.class);
        pluginDirectory = new File("plugins");
        if (!pluginDirectory.exists())
            pluginDirectory.mkdir();
        // injecting plugin folder to classpath. you can also put sql drivers there btw.
        /*try {
            Method method = URLClassLoader.class.getDeclaredMethod("addURL", new Class[]{URL.class});
            method.setAccessible(true);
            method.invoke(ClassLoader.getSystemClassLoader(), new Object[]{new File("plugins/").toURI().toURL()});
        } catch (IllegalAccessException e) {
            logger.log(e);
        } catch (InvocationTargetException e) {
            logger.log(e);
        } catch (MalformedURLException e) {
            logger.log(e);
        } catch (NoSuchMethodException e) {
            logger.log(e);
        }*/

        // Load plugin list
        logger.log("Loading plugin list...");
        refreshPluginList();
        logger.log("Done. Found " + allPlugins.size() + " Plugins");
    }

    public void refreshPluginList() {
        allPlugins = new HashMap<String, File>();
        for (File f : pluginDirectory.listFiles()) {
            if (f.isFile()) {
                try {
                    JarFile jf = new JarFile(f);
                    String name = jf.getManifest().getMainAttributes().getValue("kibi-plugin-name");
                    allPlugins.put(name, f);
                } catch (Exception e) {

                }
            }
        }
    }

    public void load(String name) throws PluginNotFoundException, InvalidPluginException, ClassNotFoundException, IOException, InstantiationException, IllegalAccessException, PluginAlreadyLoadedException {
        if (plugins.get(name) != null)
            throw new PluginAlreadyLoadedException(name);
        File plugin = allPlugins.get(name);
        if (plugin == null)
            throw new PluginNotFoundException();
        Plugin p = new Plugin(plugin, bot);
        plugins.put(name, p);
    }

    public void unload(String name) throws PluginNotFoundException {
        if (plugins.get(name) == null)
            throw new PluginNotFoundException();
        plugins.get(name).unload();
        plugins.remove(name);
        System.gc(); // Make sure the plugin gets eaten by the GC
    }

    public void enable(String name) throws PluginNotFoundException {
        if (plugins.get(name) == null)
            throw new PluginNotFoundException();
        plugins.get(name).enable();
    }

    public void disable(String name) throws PluginNotFoundException {
        if (plugins.get(name) == null)
            throw new PluginNotFoundException();
        plugins.get(name).disable();
    }

    /**
     * Loads a plugin by using its class name
     *
     * @param name The Classname
     * @param as The Plugin name
     */
    public void loadInternal(String name, String as) throws IllegalAccessException, InstantiationException, ClassNotFoundException, PluginAlreadyLoadedException, InvalidPluginException {
        try {
            if (plugins.get(as) != null)
                throw new PluginAlreadyLoadedException(as);
            Plugin plugin = new Plugin(name, bot, as);
            plugins.put(as, plugin);
        } catch (ClassCastException ex) {
            throw new InvalidPluginException("Plugin class doesn't implement IPlugin");
        }
        /*} catch (ClassCastException ex) {
            // No Plugin
        } catch (ClassNotFoundException e) {
            // No Such Class/broken Manifest
        } catch (InstantiationException e) {
            // Constructor Failed
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }*/
    }

    public List<String> getPluginList() {
        List<String> ret = new LinkedList<String>();
        for (String s : allPlugins.keySet()) {
            ret.add(s);
        }
        return ret;
    }
}
