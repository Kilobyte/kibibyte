package de.kilobyte22.app.kibibyte;

import java.io.*;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 10.03.13
 * Time: 12:34
 * To change this template use File | Settings | File Templates.
 */

// For now no real yaml
public class YamlEngine {
    public HashMap<String, String> load(File file) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String l = "";
        HashMap<String, String> ret = new HashMap<String, String>();
        while ((l = reader.readLine()) != null) {
            if (!l.equals("")) {
                String key = l.split(":")[0];
                String value = l.substring(key.length() + 1);
                ret.put(key, value);
            }
        }
        return ret;
        /*Yaml yaml = new Yaml();
        if (!file.exists()) file.createNewFile();
        return (HashMap<String, String>) new Yaml().load(new FileReader(file));*/
    }

    public void writeYaml(File file, HashMap<String, String> data) throws IOException {
        if (file.exists()) file.delete();
        file.createNewFile();
        //new Yaml().dump(data, new FileWriter(file));
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        for (String key : data.keySet()) {
            writer.write(key + ":" + data.get(key) + "\n");
        }
    }
}
