package de.kilobyte22.app.kibibyte.command;

import de.kilobyte22.app.kibibyte.Kibibyte;
import de.kilobyte22.app.kibibyte.misc.Args;
import de.kilobyte22.app.kibibyte.plugin.BotAccess;
import de.kilobyte22.lib.logging.Logger;
import org.pircbotx.Channel;
import org.pircbotx.User;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 15.02.13
 * Time: 21:32
 *
 * @author ${user}
 * @copyright Copyright ${year} ${user}
 */
public abstract class CommandHandler {
    //protected CommandOutputStream out;

    protected void print(String message) {
        bot.sendNotice(sender, message);
    }

    protected void printChan(String message) {
        bot.sendMessage(channel, message);
    }

    protected Logger logger;

    protected Channel channel;
    protected User sender;
    protected Args args;
    @Deprecated protected Kibibyte bot;
    protected BotAccess botAccess;
    private String command;

    public static final char BOLD = (char) 2;
    public static final char RESET = (char) 15;
    public static final char COLOR = (char) 3;

    //protected void log(String s) {
        //bot.sendMessage(bot.logChan, s);
    //}

    public void setData(Kibibyte b, Channel c, User s, Args a, String cmd) {
        channel = c;
        sender = s;
        args = a;
        bot = b;
        command = cmd;
        logger = new Logger("CMD:" + cmd);
    }

    public void setData(BotAccess b, Channel c, User s, Args a, String cmd) {
        channel = c;
        sender = s;
        args = a;
        botAccess = b;
        command = cmd;
        logger = new Logger("CMD:" + cmd);
    }
}
