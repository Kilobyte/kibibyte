package de.kilobyte22.app.kibibyte.command;

import de.kilobyte22.app.kibibyte.exceptions.CommandException;
import de.kilobyte22.app.kibibyte.exceptions.UsageException;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Stephan
 * Date: 17.02.13
 * Time: 12:17
 *
 * @author Stephan
 * @copyright Copyright 2013 Stephan
 */
public class BasicCommands extends CommandHandler {
    @Command(name = "test", usage = "<text>", help = "This is for testing")
    public void test() {
        String prefix = args.getNamedParam('p');
        if (prefix == null) prefix = args.getNamedParam("prefix");
        if (prefix == null) prefix = "Test:";
        String redirect = args.getNamedParam('r');
        if (redirect == null) redirect = args.getNamedParam("redirect");
        if (redirect == null) redirect = "notice";
        if (redirect.equalsIgnoreCase("notice"))
            print(prefix + " " + args.getOrError(1));
        else if (redirect.equalsIgnoreCase("channel"))
            printChan(prefix + " " + args.getOrError(1));
        //out.printLine(prefix + " " + args.getOrError(1));
    }

    //@Command(name = "nstest", usage = "<nick>", help = "Test the nickserv system")
    public void nstest() {
        String acc = bot.nickservSystem.getAccount(args.getOrError(1));
        //out.printLine("Your account: " + (acc != null ? acc : "<none>"));
    }

    @Command(name = "raw", usage = "<raw line>", help = "Sends a raw line to the server", permission = "cmd.admin.raw")
    public void raw() {
        bot.sendRawLine(args.getOrError(1));
    }

    @Command(name = "rehash", usage = "", help = "reloads the configuration file", permission = "cmd.admin.rehash")
    public void rehash() {
        bot.rehash(sender);
    }


    @Command(name = "perm", usage = "<target> (set|unset|reset) <permission>", help = "Sets, unsets or gets a permission", permission = "cmd.admin.perm")
    public void perm() {
        String name = args.getOrError(1);
        if (args.getNamedParam("account") != null || args.getNamedParam('a') != null) {
            name = bot.nickservSystem.getAccount(name);
            if (name == null)
                throw new CommandException("Invalid account");
        }
        HashMap<String, Boolean> perms = bot.permissionSystem.getPermissions(name);
        String mode = args.getOrError(2);
        String perm = args.getOrError(3);
        if (mode.equalsIgnoreCase("set")) {
            perms.remove(perm);
            perms.put(perm, true);
        } else if (mode.equalsIgnoreCase("unset")) {
            perms.remove(perm);
            perms.put(perm, false);
        } else if (mode.equalsIgnoreCase("reset")) {
            perms.remove(perm);
        } else {
            throw new UsageException();
        }
        bot.permissionSystem.setPermissions(name, perms);
    }

    @Command(name = "help", usage = "[<command>]", help = "Gets help for a command", permission = "cmd.util.help")
    public void help() {
        String cmd = args.get(1);
        if (cmd == null) {
            List<Method> commands = bot.commandManager.getCommands();
            String tmp = "";
            for (Method m : commands) {
                Command c = ((Command)m.getAnnotation(Command.class));
                if (bot.permissionSystem.hasParsedPermission(c.permission(), sender.getNick(), channel)) {
                    if (!tmp.equals("")) tmp += ", ";
                    tmp += c.name();
                }
            }
            print("Avaible commands: " + tmp);
        } else{
            Method m = bot.commandManager.getCommand(cmd);
            Command c = ((Command)m.getAnnotation(Command.class));
            if (!bot.permissionSystem.hasParsedPermission(c.permission(), sender.getNick(), channel))
                throw new CommandException("You have no permissions for that command");
            print("Usage: " + c.name() + " " + c.usage());
            print("Aliases: " + c.aliases().toString());
            print(c.help());
        }
    }

    @Command(name = "demo", usage = "", help = "demonstrates how to add a command")
    public void demoCommand() {
        // Getting an unnamed param:
        String p1 = args.get(1);
        // Getting an unnamed param, but throw an UsageException if the param isn't set:
        String p2 = args.getOrError(2);
        // Like getOrError, but only for ints:
        int i = args.getIntOrError(3);
        // Get a param set like --param <value>
        String value = args.getNamedParam("param"); // use double quotes!
        // Get a param set like -p <value>
        String anotherValue = args.getNamedParam('p'); // note the single quotes!

        // Sending a notice to the sender (this should be used for all replys which do not need to go to a channel:
        print("Hello " + sender.getNick());
        // Sending a message to the channel
        printChan("I just said Hello to " + sender.getNick());
        // Check a permission. This checks if the user has one of the permissions cmd.util.demo.somepermission, cmd.util.demo.*, cmd.util.*, cmd.* and *
        // note that it checks in that order. once it finds a set permission it will return it. this can be false or true.
        // This allows it to for example say: i give user x permission cmd.* but i exclude cmd.admin.*
        boolean hasPerm = bot.permissionSystem.hasPermission("cmd.util.demo.somepermission", sender.getNick(), channel);
    }
}
